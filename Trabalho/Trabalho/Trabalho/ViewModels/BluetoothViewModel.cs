﻿using Bluetooth;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Trabalho.ViewModels
{
    class BluetoothViewModel : ViewModelBase
    {
        public BluetoothViewModel() : base()
        {

            bluetoothBLE = CrossBluetoothLE.Current;
            adapter = CrossBluetoothLE.Current.Adapter;
            _List = new ObservableCollection<IDevice>();

        }

        #region Propriedades

        private IAdapter adapter;
        private IBluetoothLE bluetoothBLE;
        private IDevice device;

        private ObservableCollection<IDevice> _List;
        public ObservableCollection<IDevice> List
        {
            get => _List;
            set
            {
                _List = value;
                OnPropertyChanged();
            }
        }

        public bool HasList { get => _List.Count > 0; }

        #endregion


        #region Botão Compartilhar

        private Command _CompartilharCommand;
        public Command CompartilharCommand => _CompartilharCommand ?? (_CompartilharCommand = new Command(async () => await CompartilharCommandExecute()));
        async Task CompartilharCommandExecute()
        {
            try
            {
                if (IsBusy)
                    return;

                IsBusy = true;
                CompartilharCommand.ChangeCanExecute();

                if (bluetoothBLE.State == BluetoothState.Off)
                {
                    await App.Current.MainPage.DisplayAlert("Atenção", "Bluetooth desabilitado.", "OK");
                    return;
                }
                _List.Clear();

                adapter.ScanMode = ScanMode.LowLatency;
                adapter.ScanTimeout = 5000;

                adapter.DeviceDiscovered += (s, a) =>
                {
                    if (!_List.Contains(a.Device))
                    {
                        _List.Add(a.Device);
                    }
                };
                await adapter.StartScanningForDevicesAsync();

                OnPropertyChanged(nameof(HasList));
            }
            catch
            {
                await App.Current.MainPage.DisplayAlert("Ops", "Bluetooth desabilitado.", "OK");
            }
            finally
            {
                IsBusy = false;
                CompartilharCommand.ChangeCanExecute();
            }

        }

        #endregion

        private IDevice _SelectedCommand { get; set; }
        public IDevice SelectedCommand
        {
            get => _SelectedCommand; 
            set
            {
                if (_SelectedCommand != value)
                {
                    _SelectedCommand = value;
                    SelectedCommandExecute();
                }
            }
        }
        private async void SelectedCommandExecute()
        {
             device = SelectedCommand;

            var result = await App.Current.MainPage.DisplayAlert("AVISO", "Deseja se conectar a esse dispositivo?", "Conectar", "Cancelar");
            
            if (!result || !ConnectParameters.None.AutoConnect)
                return;

            await adapter.StopScanningForDevicesAsync();

            try
            {
                await adapter.ConnectToDeviceAsync(device);
                await App.Current.MainPage.DisplayAlert("Conectado", "Status:" + device.State , "OK");
            }
            catch (DeviceConnectionException ex)
            {
                await App.Current.MainPage.DisplayAlert("Erro", ex.Message, "OK");
            }
        }
    }
}
