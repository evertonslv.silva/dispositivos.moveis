﻿using Captura;
using Plugin.AudioRecorder;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Trabalho.ViewModels
{
    class CaptureViewModel : ViewModelBase
    {

        public CaptureViewModel() : base()
        {
        }

        public bool HasFoto { get; private set; }
        public AudioRecorderService recorder { get; private set; }

        #region Botão Capturar Foto
        private Command _CapturarFotoCommand;
        public Command CapturarFotoCommand => _CapturarFotoCommand ?? (_CapturarFotoCommand = new Command(async () => await CapturarFotoCommandExecute()));
        async Task CapturarFotoCommandExecute()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await App.Current.MainPage.DisplayAlert("Nenhuma Camera", ":( Nenhuma camera encontrada.", "OK");
                return;
            }

            var armazenamento = new StoreCameraMediaOptions()
            {
                SaveToAlbum = true,
                Name = "MinhaFotoXamarin.jpg"
            };

            var foto = await CrossMedia.Current.TakePhotoAsync(armazenamento);

            HasFoto = foto != null;

        }

        #endregion

        #region Botão Gravar video
        private Command _GravarVideoCommand;
        public Command GravarVideoCommand => _GravarVideoCommand ?? (_GravarVideoCommand = new Command(async () => await GravarVideoCommandExecute()));
        async Task GravarVideoCommandExecute()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsTakeVideoSupported || !CrossMedia.Current.IsCameraAvailable)
            {
                await App.Current.MainPage.DisplayAlert("Ops", "Nenhuma câmera detectada.", "OK");

                return;
            }

            var file = await CrossMedia.Current.TakeVideoAsync(
                new StoreVideoOptions
                {
                    SaveToAlbum = true,
                    Directory = "Demo",
                    Quality = VideoQuality.Medium
                });
        }

        #endregion

        #region Botão Gravar audio
        private Command _GravarAudioCommand;
        public Command GravarAudioCommand => _GravarAudioCommand ?? (_GravarAudioCommand = new Command(async () => await GravarAudioCommandExecute()));

        async Task GravarAudioCommandExecute()
        {

            if (recorder == null)
            {
                recorder = new AudioRecorderService
                {
                    StopRecordingOnSilence = true, // Para a gravação após 2 segundos (padrão)
                    StopRecordingAfterTimeout = true,  // Para a gravação depois de um tempo limite máximo (definido abaixo)
                    TotalAudioTimeout = TimeSpan.FromSeconds(15) // O áudio irá parar de gravar após 15 segundos 
                };
            }


            try
            {
                if (!recorder.IsRecording)
                {
                    await recorder.StartRecording();
                }
                else
                {
                    await recorder.StopRecording();
                }
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Ops", ex.Message, "OK");
            }
        }

        #endregion

    }
}
