﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trabalho.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Trabalho.Views
{
	public partial class CapturePage : ContentPage
	{
		public CapturePage()
		{
			InitializeComponent ();
            BindingContext = new CaptureViewModel();
		}
	}
}