﻿using Bluetooth;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Trabalho.ViewModels
{
    class BluetoothViewModel : ViewModelBase
    {
        public BluetoothViewModel() : base()
        {
            _Ble = CrossBluetoothLE.Current;
            _Adapter = CrossBluetoothLE.Current.Adapter;
            _List = new ObservableCollection<IDevice>();
        }

        #region Propriedades

        private IAdapter _Adapter;
        private IBluetoothLE _Ble;
        private IDevice _Device;

        private ObservableCollection<IDevice> _List;
        public ObservableCollection<IDevice> List
        {
            get => _List;
            set
            {
                _List = value;
                OnPropertyChanged();
            }
        }

        public bool HasList { get => _List.Count > 0; }

        #endregion

        #region Botão Buscar dispositivos

        private Command _BuscarCommand;
        public Command BuscarCommand => _BuscarCommand ?? (_BuscarCommand = new Command(async () => await BuscarCommandExecute()));
        async Task BuscarCommandExecute()
        {
            try
            {
                if (IsBusy)
                    return;

                IsBusy = true;
                BuscarCommand.ChangeCanExecute();

                if (_Ble.State == BluetoothState.Off)
                {
                    await App.Current.MainPage.DisplayAlert("Atenção", "Bluetooth desabilitado.", "OK");
                    return;
                }

                _List.Clear();

                _Adapter.ScanMode = ScanMode.LowLatency;
                _Adapter.ScanTimeout = 2000;

                _Adapter.DeviceDiscovered += (s, a) =>
                {
                    if (!_List.Contains(a.Device))
                    {
                        _List.Add(a.Device);
                    }
                };

                await _Adapter.StartScanningForDevicesAsync();

                OnPropertyChanged(nameof(HasList));
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Ops", ex.Message.ToString(), "OK");
            }
            finally
            {
                IsBusy = false;
                BuscarCommand.ChangeCanExecute();
            }

        }

        #endregion

        #region Botão Conectar Bluetooth
        private IDevice _SelectedCommand { get; set; }
        public IDevice SelectedCommand
        {
            get => _SelectedCommand;
            set
            {
                if (_SelectedCommand != value)
                {
                    _SelectedCommand = value;
                    SelectedCommandExecute();
                }
            }
        }
        private async void SelectedCommandExecute()
        {
            _Device = SelectedCommand;

            if (_Device != null)
            {
                var result = await App.Current.MainPage.DisplayAlert("AVISO", "Deseja se conectar a esse dispositivo?", "Conectar", "Cancelar");

                if (!result)
                    return;

                //await adapter.StopScanningForDevicesAsync();

                try
                {
                    await _Adapter.ConnectToDeviceAsync(_Device);
                    //await adapter.ConnectToKnownDeviceAsync(device.Id);
                    await App.Current.MainPage.DisplayAlert("Conectado", $"Status: {_Device.Name} - {_Device.State}", "OK");

                }
                catch (DeviceConnectionException ex)
                {
                    await App.Current.MainPage.DisplayAlert("Erro", ex.Message, "OK");
                }
            }
        }

        #endregion
    }
}
