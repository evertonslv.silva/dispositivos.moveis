﻿using Trabalho.ViewModels;
using Xamarin.Forms;

namespace Trabalho.Views
{
	public partial class BluetoothPage : ContentPage
	{
		public BluetoothPage ()
		{
			InitializeComponent ();
            BindingContext = new BluetoothViewModel();
		}
	}
}