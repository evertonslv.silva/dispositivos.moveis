﻿using Trabalho.ViewModels;
using Xamarin.Forms;

namespace Trabalho.Views
{
	public partial class HelloWorldPage : ContentPage
	{
		public HelloWorldPage()
		{
			InitializeComponent ();
            DisplayAlert("Hello World", "Hello World", "OK");
            BindingContext = new HelloWorldViewModel();
		}
	}
}