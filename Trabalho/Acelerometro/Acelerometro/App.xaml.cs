﻿using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;
using System;
using Trabalho.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Acelerometro
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new AcelerometroPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            CrossDeviceMotion.Current.Start(MotionSensorType.Accelerometer, MotionSensorDelay.Default);
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
