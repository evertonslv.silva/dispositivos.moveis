﻿using System;
using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;
using Trabalho.ViewModels;
using Xamarin.Forms;

namespace Trabalho.Views
{
	public partial class AcelerometroPage : ContentPage
	{
		public AcelerometroPage()
		{
			InitializeComponent ();
            BindingContext = new AcelerometroViewModel();
		}
    }
}