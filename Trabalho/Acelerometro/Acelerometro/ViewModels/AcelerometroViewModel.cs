﻿using Acelerometro;
using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Trabalho.ViewModels
{
    class AcelerometroViewModel : ViewModelBase
    {
        public AcelerometroViewModel() : base()
        {
            CrossDeviceMotion.Current.SensorValueChanged += (s, a) =>
            {
                if (a.SensorType == MotionSensorType.Accelerometer)
                {
                    _eixoX = ((MotionVector)a.Value).X.ToString("F");
                    _eixoY = ((MotionVector)a.Value).Y.ToString("F");
                    _eixoZ = ((MotionVector)a.Value).Z.ToString("F");

                    OnPropertyChanged(nameof(GetValueSensor));

                    SetPosicao();
                }
            };
        }

        #region Propriedades

        private string _eixoX;
        public string EixoX
        {
            get => _eixoX;
            set
            {
                _eixoX = value;
                OnPropertyChanged();
            }
        }

        private string _eixoY;
        public string EixoY
        {
            get => _eixoY;
            set
            {
                _eixoY = value;
                OnPropertyChanged();
            }
        }

        private string _eixoZ;
        public string EixoZ
        {
            get => _eixoZ;
            set
            {
                _eixoZ = value;
                OnPropertyChanged();
            }
        }

        public string GetValueSensor
        {
            get => $"X:{EixoX} - Y:{EixoY} - Z:{EixoZ}";
        }

        public int PosicaoX { get; private set; }
        public int PosicaoY { get; private set; }

        #endregion

        public void SetPosicao()
        {
            if (float.Parse(EixoX) > 0)
            {
                if (PosicaoX > -120)
                    PosicaoX -= 10;
            }
            else
            {
                if (PosicaoX < 120)
                    PosicaoX += 10;
            }

            if (float.Parse(EixoY) > 0)
            {
                if (PosicaoY < 200)
                    PosicaoY += 10;
            }
            else
            {
                if (PosicaoY > -200)
                    PosicaoY -= 10;
            }

            OnPropertyChanged(nameof(PosicaoX));
            OnPropertyChanged(nameof(PosicaoY));
        }
    }
}
