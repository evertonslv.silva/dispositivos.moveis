﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPS.Providers
{
    public interface ISQLiteDatabasePathProviders
    {
        string GetDatabasePath();
    }
}
