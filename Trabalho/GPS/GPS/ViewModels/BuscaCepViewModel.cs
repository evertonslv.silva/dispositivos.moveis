﻿using GPS;
using GPS.Data;
using GPS.Data.Dtos;
using GPS.Views;
using System;
using System.Threading.Tasks;
using Trabalho.Clients;
using Xamarin.Forms;

namespace Trabalho.ViewModels
{
    class BuscaCepViewModel : ViewModelBase
    {

        private CepDto _Cep = null;

        public BuscaCepViewModel() : base() { }

        #region Propriedades

        private string _CepBusca;
        public string CepBusca
        {
            get => _CepBusca;
            set
            {
                _CepBusca = value;
                OnPropertyChanged();
            }
        }
        public string CEP { get => _Cep?.Cep; }
        public string Logradouro { get => _Cep?.Logradouro; }
        public string Bairro { get => _Cep?.Bairro; }
        public string Localidade { get => _Cep?.Localidade; }
        public string Uf { get => _Cep?.UF; }
        public bool HasCep { get => _Cep != null; }

        #endregion

        #region Botão Buscar Cep

        private Command _BuscarCommand;
        public Command BuscarCommand => _BuscarCommand ?? (_BuscarCommand = new Command(async () => await BuscarCommandExecute()));
        async Task BuscarCommandExecute()
        {
            try
            {

                if (IsBusy)
                    return;

                IsBusy = true;
                BuscarCommand.ChangeCanExecute();
                AdicionarCommand.ChangeCanExecute();

                var result = await ViaCepHttpClients.Current.BuscarCep(_CepBusca);

                _Cep = new CepDto
                {
                    Bairro = result.bairro,
                    Cep = result.cep,
                    Logradouro = result.logradouro,
                    Localidade = result.localidade,
                    UF = result.uf,
                    Id = Guid.NewGuid()
                };

                OnPropertyChanged(nameof(HasCep));
                OnPropertyChanged(nameof(CEP));
                OnPropertyChanged(nameof(Logradouro));
                OnPropertyChanged(nameof(Localidade));
                OnPropertyChanged(nameof(Uf));
                OnPropertyChanged(nameof(Bairro));

            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Alerta", e.Message, "OK");
            }
            finally
            {
                IsBusy = false;

                BuscarCommand.ChangeCanExecute();
                AdicionarCommand.ChangeCanExecute();
            }
        }

        #endregion
        
        #region Adicionar CEP

        private Command _AdicionarCommand;
        public Command AdicionarCommand => _AdicionarCommand ?? (_AdicionarCommand = new Command(async () => await AdicionarCommandExecute()));
        async Task AdicionarCommandExecute()
        {
            try
            {
                if (IsBusy)
                    return;

                IsBusy = true;
                BuscarCommand.ChangeCanExecute();
                AdicionarCommand.ChangeCanExecute();

                DataBase.Current.Save(_Cep);

                await App.Current.MainPage.DisplayAlert("", "CEP adicionado!", "OK");

                _Cep = null;
                _CepBusca = "";

                OnPropertyChanged(nameof(HasCep));
                OnPropertyChanged(nameof(CepBusca));

            }
            catch
            {
                await App.Current.MainPage.DisplayAlert("A não", "erro", "OK");
            }
            finally
            {
                IsBusy = false;
                BuscarCommand.ChangeCanExecute();
                AdicionarCommand.ChangeCanExecute();
            }
        }

        #endregion

        #region Mostrar histórico

        private Command _HistoricoCommand;
        public Command HistoricoCommand => _HistoricoCommand ?? (_HistoricoCommand = new Command(async () => await HistoricoCommandExecute()));
        async Task HistoricoCommandExecute()
        {
            await PushAsync(new HistoricoPage());
        }

        #endregion
    }
}
