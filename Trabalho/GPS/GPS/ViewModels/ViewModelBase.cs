﻿using GPS;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Trabalho.ViewModels
{
    class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if(PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private bool _Isbusy = false;
        public bool IsBusy
        {
            get => _Isbusy;
            set
            {
                _Isbusy = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsNotBusy));
            }
        }

        public bool IsNotBusy { get => !_Isbusy; }

        protected async Task PushAsync(Page page, bool animated = true)
        {
           await App.Current.MainPage.Navigation.PushAsync(page, animated);
        }

    }
}
