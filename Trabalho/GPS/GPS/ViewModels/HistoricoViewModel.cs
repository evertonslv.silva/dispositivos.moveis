﻿using GPS.Data;
using GPS.Data.Dtos;
using GPS.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Trabalho.ViewModels;
using Xamarin.Forms;

namespace GPS.ViewModels
{
    class HistoricoViewModel : ViewModelBase
    {

        public HistoricoViewModel() : base()
        {
            foreach (CepDto cep in DataBase.Current.CepGetAll())
            {
                Ceps.Add(cep);
            }
        }

        #region Mostrar no mapa

        public ObservableCollection<CepDto> Ceps { get; private set; } = new ObservableCollection<CepDto>();
        private CepDto cep;

        private CepDto _SelectedCommand { get; set; }
        public CepDto SelectedCommand
        {
            get => _SelectedCommand;
            set
            {
                if (_SelectedCommand != value)
                {
                    _SelectedCommand = value;
                    SelectedCommandExecute();
                }
            }
        }
        private async void SelectedCommandExecute()
        {
            cep = SelectedCommand;
            string endereco = GetEndereco(cep);

            await PushAsync(new MapaPage(endereco, cep.Localidade));
        }

        private string GetEndereco(CepDto cep)
        {
            string endereco = cep.Logradouro;

            if (!string.IsNullOrWhiteSpace(cep.Bairro))
                endereco += ", " + cep.Bairro;

            if (!string.IsNullOrWhiteSpace(cep.Localidade) && !string.IsNullOrWhiteSpace(cep.UF))
                endereco += $", {cep.Localidade}/{cep.UF}";

            endereco += ", Brazil";

            return endereco;
        }

        #endregion
    }


}
