﻿using GPS.Data.Dtos;
using GPS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace GPS.Views
{
    public partial class MapaPage : ContentPage
    {
        private string Endereco { get; set; }

        private string Cidade { get; set; }

        public MapaPage(string end, string cid)
        {
            InitializeComponent();
            Endereco = end;
            Cidade = cid;

            AtualizaMapa();
        }

        private async void AtualizaMapa()
        {
            map.MyLocationEnabled = true;
            map.UiSettings.MyLocationButtonEnabled = true;

            Geocoder coder = new Geocoder();
            var positions = (await coder.GetPositionsForAddressAsync(Endereco)).ToList();

            Pin _pinTokyo = new Pin()
            {
                Type = PinType.Place,
                Label = Cidade,
                Address = Endereco,
                Position = new Position(positions[0].Latitude, positions[0].Longitude)
            };

            _pinTokyo.IsDraggable = true;
            map.Pins.Add(_pinTokyo);
            map.SelectedPin = _pinTokyo;
            map.MoveToRegion(MapSpan.FromCenterAndRadius(_pinTokyo.Position, Distance.FromMeters(5000)), true);
        }
    }
}