﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trabalho.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Trabalho.Views
{
	public partial class BuscaCepPage : ContentPage
	{
		public BuscaCepPage ()
		{
			InitializeComponent ();
            BindingContext = new BuscaCepViewModel();
		}
	}
}