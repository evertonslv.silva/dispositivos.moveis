﻿using GPS.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GPS.Views
{
    public partial class HistoricoPage : ContentPage
    {

        public HistoricoPage()
        {
            InitializeComponent();

            BindingContext = new HistoricoViewModel();
        }
    }
}
