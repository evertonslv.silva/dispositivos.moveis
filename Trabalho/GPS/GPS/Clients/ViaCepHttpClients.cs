﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho.Clients
{
    class ViaCepHttpClients
    {
        private static Lazy<ViaCepHttpClients> _Lazy = new Lazy<ViaCepHttpClients>(() => new ViaCepHttpClients());
        public static ViaCepHttpClients Current { get => _Lazy.Value; }

        private ViaCepHttpClients()
        {
            _HttpClient = new HttpClient();
        }

        private readonly HttpClient _HttpClient;

        public async Task<BuscaCepResult> BuscarCep(string cep)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(cep))
                    throw new InvalidOperationException("Informe o CEP");

                using (var client = new HttpClient())
                {
                    using (var response = await client.GetAsync($"http://www.viacep.com.br/ws/{cep}/json/"))
                    {
                        if (!response.IsSuccessStatusCode)
                            throw new InvalidOperationException("Algo de errado!");

                        var result = await response.Content.ReadAsStringAsync();

                        if (string.IsNullOrWhiteSpace(result))
                            throw new InvalidOperationException("Algo de errado!");

                        return JsonConvert.DeserializeObject<BuscaCepResult>(result);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public class BuscaCepResult
        {
            public string cep { get; set; }
            public string logradouro { get; set; }
            public string complemento { get; set; }
            public string bairro { get; set; }
            public string localidade { get; set; }
            public string uf { get; set; }
            public string unidade { get; set; }
            public string ibge { get; set; }
            public string gia { get; set; }
        }

    }
}
