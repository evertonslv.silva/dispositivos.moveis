﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPS.Data.Dtos
{
    [Table("Cep")]

    sealed class CepDto
    {
        [PrimaryKey]
        public Guid Id { get; set; }
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string UF { get; set; }

        [Ignore]
        public string Detalhes
        {
            get
            {
                string detalhes = Logradouro;

                if (!string.IsNullOrWhiteSpace(Bairro))
                    detalhes += $", {Bairro}";

                if (!string.IsNullOrWhiteSpace(Localidade) && string.IsNullOrWhiteSpace(UF))
                    detalhes += $", {Localidade}/{UF}";

                return detalhes;

            }
        }
    }
}
