﻿using GPS.Data.Dtos;
using GPS.Providers;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GPS.Data
{
    class DataBase
    {
        private static Lazy<DataBase> _Lazy = new Lazy<DataBase>(() => new DataBase());

        public static DataBase Current { get => _Lazy.Value; }

        private DataBase()
        {
            var dbPath = DependencyService.Get<ISQLiteDatabasePathProviders>().GetDatabasePath();
            _SQLiteConnection = new SQLiteConnection(dbPath);
            _SQLiteConnection.CreateTable<CepDto>();
        }

        private readonly SQLiteConnection _SQLiteConnection;

        public bool Save(CepDto cep) => _SQLiteConnection.InsertOrReplace(cep) > 0;

        public List<CepDto> CepGetAll() => _SQLiteConnection.Table<CepDto>().ToList();
        public CepDto CepGet(Guid id) => _SQLiteConnection.Find<CepDto>(id);


    }
}
