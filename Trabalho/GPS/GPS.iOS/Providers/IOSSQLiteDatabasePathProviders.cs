﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using GPS.iOS.Providers;
using GPS.Providers;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(IOSSQLiteDatabasePathProviders))]

namespace GPS.iOS.Providers
{
    class IOSSQLiteDatabasePathProviders : ISQLiteDatabasePathProviders
    {
        public IOSSQLiteDatabasePathProviders() { }

        public string GetDatabasePath()
        {
            var db = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "..", "Library", "Databases");

            if (!Directory.Exists(db))
                Directory.CreateDirectory(db);

            return Path.Combine(db, "cep.db3");
        }
    }
}