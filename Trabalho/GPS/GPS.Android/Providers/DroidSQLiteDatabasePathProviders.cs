﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GPS.Droid.Providers;
using GPS.Providers;
using Xamarin.Forms;
using Environment = System.Environment;

[assembly: Dependency(typeof(DroidSQLiteDatabasePathProviders))]

namespace GPS.Droid.Providers
{
    class DroidSQLiteDatabasePathProviders : ISQLiteDatabasePathProviders
    {

        public DroidSQLiteDatabasePathProviders() { }

        public string GetDatabasePath()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "cep.db3");
        }
    }
}